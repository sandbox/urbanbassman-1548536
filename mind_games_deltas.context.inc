<?php
/**
 * @file
 * mind_games_deltas.context.inc
 */

/**
 * Implements hook_context_default_contexts().
 */
function mind_games_deltas_context_default_contexts() {
  $export = array();

  $context = new stdClass();
  $context->disabled = FALSE; /* Edit this to true to make a default context disabled initially */
  $context->api_version = 3;
  $context->name = 'front_page';
  $context->description = 'Front page layout';
  $context->tag = 'layout';
  $context->conditions = array(
    'path' => array(
      'values' => array(
        '<front>' => '<front>',
      ),
    ),
  );
  $context->reactions = array(
    'delta' => array(
      'delta_template' => 'no_columns',
    ),
  );
  $context->condition_mode = 1;

  // Translatables
  // Included for use with string extractors like potx.
  t('Front page layout');
  t('layout');
  $export['front_page'] = $context;

  return $export;
}
